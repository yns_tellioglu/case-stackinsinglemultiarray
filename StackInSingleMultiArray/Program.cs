﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StackApp
{
    public class StackUsingApp
    {

        public class StackInSingleMultiArray
        {
            private static int _arrSize = 0;
            private readonly int[] _arr = new int[0];
            private readonly int[] stacksSlots = new int[0];
            private readonly int stackSize = 0;
            public StackInSingleMultiArray(int stackLenght, int arraySize)
            {
                _arrSize = arraySize;
                _arr = new int[_arrSize];
                stackSize = _arrSize / stackLenght;
                stacksSlots = new int[stackSize];

                for (int i = 0; i < stackLenght; i++)
                {
                    stacksSlots[i] = i * stackSize;
                }

            }

            public void Push(int stackNum, int data)
            {
                _arr[stacksSlots[stackNum]++] = data;
            }

            public int Pop(int stackNum)
            {
                return _arr[--stacksSlots[stackNum]];
            }


            public void PrintStackValue(int stackNum)
            {
                for (int i = stackNum * stackSize; i < stacksSlots[stackNum]; i++)
                {
                    Console.WriteLine(_arr[i]);
                }
            }
        }
 
        public static void Main(string[] args)
        { 
            int stackLenght = 3, arraySize = 15;

            StackInSingleMultiArray singleStackArray = new StackInSingleMultiArray(stackLenght, arraySize);

            singleStackArray.Push(2, 15);
            singleStackArray.Push(1, 45);
            singleStackArray.Push(2, 49);
            singleStackArray.Push(1, 60);

            singleStackArray.Push(1, 17);
            singleStackArray.Push(2, 58);

            singleStackArray.Push(0, 11);
            singleStackArray.Push(1, 39);
            singleStackArray.Push(0, 7);

            singleStackArray.PrintStackValue(2);
            singleStackArray.PrintStackValue(0);

            Console.WriteLine(string.Format("3. Stack son eklenen değer : {0}", singleStackArray.Pop(2)));
            Console.WriteLine(string.Format("2. Stack son eklenen değer : {0}", singleStackArray.Pop(1)));
            Console.WriteLine(string.Format("1. Stack son eklenen değer : {0}", singleStackArray.Pop(0)));

        }
    }
}
