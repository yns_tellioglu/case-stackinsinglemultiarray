## PermutasyonAppApp

This project was generated with C# , Console Application

## System Requirements

C# , .NET Framework version 4.6.* , Visual Studio IDE 

## Solution

* The program starts by taking the dimension stack Lenght and array Size
* Stacker is created in the array as many as the entered stacks
* For example, if 3 stacks are created, a stack is added from the beginning of the array to the second and the last stack is added from the end and grows.

* Works with 3 functions called Pop , Push and PrintStackValue

* Pop : It expects the stacking and the value it will add from you. Adds the entered value to the stack

* Push : Removes the last added element in the entered stack FIFO

* PrintStackValue : returns the values ​​in the stack you entered







